/**
 *
 * Given is the following FizzBuzz application which counts from 1 to 100 and outputs either the
 * corresponding number or the corresponding text if one of the following rules apply.
 *
 * Rules:
 *  - dividable by 3 without a remainder -> Fizz
 *  - dividable by 5 without a remainder -> Buzz
 *  - dividable by 3 and 5 without a remainder -> FizzBuzz
 *
 * ACCEPTANCE CRITERIA:
 *
 * Please refactor this code so that it can be easily extended with other rules, such as
 * - "if it is dividable by 7 without a remainder output Bar"
 * - "if multiplied by 10 is larger than 100 output Foo"
 */

/**
 * In a case of refactoring without changing environment this implementation is enough clear, readable and extendable.
 * If there is need to totally rewrite in a right way, I'd like to implement it with typescript.
 * In this case will be created interface and family of classes for each rule, which implements same interface.
 * Main logic will be stored in main class (service) which have possibility to add/remove rules.
 *
 * In this case will be implemented open-closed principle with strict types.
 * Adding new rules through creating new `RuleClass` and adding it to a list with FizzBuzz().addRule(new RuleClass()).
 */
const criteriaSet = new Set([
    { callback: (index) => index % 3 === 0, output: "Fizz" },
    { callback: (index) => index % 5 === 0, output: "Buzz" },
    { callback: (index) => index % 7 === 0, output: "Bar" },
    { callback: (index) => index * 10 > 100, output: "Foo" },
]);

function fizzbuzz(limit = 100) {
  for (let i = 1; i <= limit; i++) {
    let output = "";

    criteriaSet.forEach((criteria) => {
      if (criteria.callback(i)) {
        output += criteria.output;
      }
    });

    console.log(`${i}: ${output || i}`);
  }
}

fizzbuzz();
