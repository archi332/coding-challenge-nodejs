/**

Print a pyramid!

    *
   ***
  *****
 *******
*********

ACCEPTANCE CRITERIA:

Write a script to output pyramid of given size to the console (with leading spaces).

*/
/**
 * Provided solution is more common. Readable and more simple.
 * Alternatively both nested `for loops` can be replaced with String.prototype.repeat(), if more preferable.
 * Like: console.log(' '.repeat(size - i) + '*'.repeat(i * 2 - 1))
 */
function pyramid(size = 5) {
  for (let i = 1; i <= size; i++) {
    let row = '';

    for (let j = 0; j < size - i; j++) {
      row += ' ';
    }

    for (let k = 0; k < i * 2 - 1; k++) {
      row += '*';
    }
    console.log(row)
  }
  console.log("A beautiful pyramid")
}

pyramid(5)
